%{

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <resolv.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <syslog.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>
#include <math.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <pthread.h>
#include <sys/wait.h>
#include <syslog.h>
#include <getopt.h>
#include <mysql.h>

void inputToFlex(char *buf, int *result, size_t  max_size);

#undef YY_INPUT
#define YY_INPUT(buf, result, max_size) inputToFlex(buf, &result, max_size)

typedef enum { 
    TOKEN_UNKNOWN = 0, 
    TOKEN_CONFIG, 
    TOKEN_DATA, 
    TOKEN_POWER
} TOKEN_ENUM;

%}

CONFIG          \x02\x43
DATA	        \x02\x44
PWR             \x02\x50
BYTE            \x10(\x82|\x83|\x90)|[\x00-\xFF]
TEMPLATE_ID     \x11

    /* Onderdrukking flex warnings */
%option nounput
%option noinput

%% Rules

    /* CONFIG */
{CONFIG}{TEMPLATE_ID}{BYTE}{40}{BYTE}{3}{BYTE}{3}{BYTE}{2}{BYTE}{4}{BYTE}{1}{BYTE}{1}[\x03] {
        return TOKEN_CONFIG;
    }

    /* DATA */
{DATA}{BYTE}{3}{BYTE}{40}{BYTE}{6}{BYTE}{6}{BYTE}{2}{BYTE}{1}[\x03] { 
        return TOKEN_DATA; 
    }

    /* POWER */
{PWR}{BYTE}{1}{BYTE}{1}{BYTE}{1}[\x03] {
        return TOKEN_POWER; 
    }

    /* Catch all voor alle andere data */
.   { 
        // Eatup all invalid tokens - print msg
        // Don't return, handle invalid char here.
        printf("Invalid token: [%.2X]\n", (unsigned char)yytext[0]);
        //return TOKEN_UNKNOWN; 
    }

%%

//
// Defines & macro's
//
#define OK                                  1
#define NOK                                 0  
#define SERVER_PORT                         7000
#define HEATBEAT_INTERVAL_SEC               1
#define RECVBUFFERSIZE                      2048

#define DATABASE_SERVER                     "127.0.0.1"
#define DATABASE_USER                       "footpod"
#define DATABASE_PASSWORD                   "footpod"
#define DATABASE_SCHEMA                     "FootpodAnalyser"

//
// Typedef's 
//
typedef struct
{
    signed int x,y,z;
} ACCELEROMETER_STRUCT;


typedef struct {
    unsigned long packetCounter;
    unsigned int pressure[20];
    ACCELEROMETER_STRUCT accelero[2];
    signed int temperature;
    unsigned char crc;
} DATAPACKET_STRUCT;


typedef struct
{
    unsigned char batteryLevel;
    unsigned char onCharger;
    unsigned char crc;
} POWERPACKET_STRUCT;


typedef struct 
{
    signed char offset;
    unsigned char gain;
} PESSURE_CALIBRATION_STRUCT;


typedef struct
{
    signed char offsetX, offsetY, offsetZ;
} ACCELERO_CALIBRATION_STRUCT;


typedef struct
{
    signed char offset;
    unsigned char gain;
} TEMPERATURE_CALIBRATION_STRUCT;

typedef enum
{
    LEFT_PAD = 0, 
    RIGHT_PAD
} PAD_ENUM;

typedef enum
{
    TOE = 0, 
    HEEL
} ACCELERO_SELECT_ENUM;

typedef struct
{
    PESSURE_CALIBRATION_STRUCT presureCalVals[20];
    ACCELERO_CALIBRATION_STRUCT acceleroCalVals[2];
    TEMPERATURE_CALIBRATION_STRUCT tempCalVals;
    unsigned long serialNr;
    PAD_ENUM padType;
    unsigned char crc;
    unsigned char isValid;
} CONFIGPACKET_TYPE_0X11_STRUCT;

typedef enum
{
    OUTPUT_NONE = 0,
    OUTPUT_RAW = 1,
    OUTPUT_DECODED = 2,
    OUTPUT_SQL = 3
} DEBUG_OUTPUT_ENUM;

// 
// Global variables
//
unsigned char recvBuffer[RECVBUFFERSIZE];
int hbdone = 0;
int bufferIndex = 0;
int psock = 0;
DEBUG_OUTPUT_ENUM debugLevel = OUTPUT_NONE;
CONFIGPACKET_TYPE_0X11_STRUCT currentConfigPacket;
char sessionUuid[20];
unsigned long prevPacketCounter;

//// Session time
//struct timespec sessionStart, sessionStop;

// Session time
struct timeval sessionStart, sessionStop;

//
// Function proto's 
//
void handlePowerPacket(unsigned char *data, int leng, POWERPACKET_STRUCT *powerPacket);
void handleDataPacket(unsigned char *data, int leng, DATAPACKET_STRUCT *dataPacket);
void handleConfigPacket(unsigned char *data, int leng, CONFIGPACKET_TYPE_0X11_STRUCT *configPacket);

// logging
void logDataPacket(DATAPACKET_STRUCT *dataPacket);
void logPowerPacket(POWERPACKET_STRUCT *powerPacket);
void logConfigPacket(CONFIGPACKET_TYPE_0X11_STRUCT *configPacket);

// db
MYSQL *connectToDatabase(void);
void storeDataPacket(MYSQL *connection, DATAPACKET_STRUCT *dataPacket);
void storeConfigPacket(MYSQL *connection, CONFIGPACKET_TYPE_0X11_STRUCT *configPacket);
void storePowerPacket(MYSQL *connection, POWERPACKET_STRUCT *powerPacket);

// uuid
void genuuid(char *uuid);

// profiling
struct timespec timespecdiff(struct timespec start, struct timespec end);


//
// Redefine input voor flex. Dit is een invulling voor de YY_INPUT macro
//
void inputToFlex(char *buf, int *result, size_t max_size)
{
    int bytes;
    if( (bytes = recv(psock, recvBuffer, RECVBUFFERSIZE, 0)) )
    {
        memcpy(buf, recvBuffer, bytes );
        *result = bytes;
        if( debugLevel == OUTPUT_RAW)
            printf("grabbing [%d] bytes from tcp buffer\n", bytes);
    }
    else
    {
        *result = YY_NULL;
        if( debugLevel == OUTPUT_RAW)
            printf("empty tcp buffers, bye ...\n");
    }
 }

//
// Redefine yywrap() -> altijd terminate yylex()
//
int yywrap()
{
    if( debugLevel == OUTPUT_RAW)
        printf("yywrap()\n");
    return(1); 
}

//
// handle heartbeat
//
void *handleHeartbeat(void *sock)
{
    while(!hbdone)
    {
        //tx the heartbeat
        send((int)sock, "Hello Kitty", sizeof("Hello Kitty"),0);
        sleep(HEATBEAT_INTERVAL_SEC);
    }
    
    return NULL;
}

//
// 
//
void signalHandler(int sig)
{
    if( sig == SIGCHLD )
    {
        // Suspends executie van huidige process
        // totdat alle child gestopt zijn
        // (0) -> any child !
        wait(0);
    }
}


//
//
//
void handleConnection(int sock)
{   
    int done;
    int idx;
 
    DATAPACKET_STRUCT dataPacket;
    POWERPACKET_STRUCT powerPacket;

    psock = sock;

    // 
    // Start hardbeat thread
    //
    hbdone = 0;
    pthread_t hbThread;
    if( pthread_create(&hbThread, NULL, handleHeartbeat, (void*)sock) != 0 )
    {
        fprintf(stderr, "Failed to create heartbeat thread\n");
    } 

    //
    //
    //
    MYSQL *connection = connectToDatabase();
    if(!connection)
    {
        fprintf(stderr, "Database connection failed. Bye ...\n");
    }

    // 
    // Generate session uuid
    genuuid(sessionUuid);

    prevPacketCounter = 0;

    // Capture session start time
    //clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &sessionStart);
    gettimeofday(&sessionStart, NULL);

    // 
    // Receive bytes till client disconnects. After that, kill process
    //
    done = NOK;
    int retval;
    while( (retval = yylex()) )
    {
        switch(retval)
        {
            case TOKEN_DATA:
                if( debugLevel == OUTPUT_RAW)
                {
                    printf("DATA RAW   : ");
                    for(idx = 0; idx < yyleng; idx++)
                    {
                        printf("%.2X",(unsigned char)yytext[idx]);
                    }
                    printf("\n");
                }
                if( currentConfigPacket.isValid )
                {
                    handleDataPacket((unsigned char *)yytext, yyleng, &dataPacket);
                    storeDataPacket(connection, &dataPacket);
                    
                    // Test for and handle for packetjumps
                    if( prevPacketCounter != dataPacket.packetCounter-1 )
                    {
                        fprintf(stderr, "Packetcounter jump detected. "
                                        "(0x%.4lX -> 0x%.4lX, %ld packets are missing!)\n",
                            prevPacketCounter, dataPacket.packetCounter, 
                            (dataPacket.packetCounter - prevPacketCounter));
                    }
                    prevPacketCounter = dataPacket.packetCounter;

                    if( debugLevel == OUTPUT_DECODED )
                    {
                        logDataPacket(&dataPacket);
                    }
                }
                else
                {
                    printf("FYI: no CONFIG received yet, rejecting DATA packet\n");
                }
                break;

            case TOKEN_POWER:
                if( debugLevel == OUTPUT_RAW )
                {
                    printf("PWR RAW    : ");
                    for(idx = 0; idx < yyleng; idx++)
                    {
                        printf("%.2X",(unsigned char)yytext[idx]);
                    }
                    printf("\n");
                }
                if(currentConfigPacket.isValid )
                {
                    handlePowerPacket((unsigned char *)yytext, yyleng, &powerPacket);
                    storePowerPacket(connection, &powerPacket);
                    if( debugLevel == OUTPUT_DECODED )
                    {
                        logPowerPacket(&powerPacket);
                    }
                }
                else
                {
                    printf("No CONFIG received yet, rejecting POWER packet\n");
                }
                break;

            case TOKEN_CONFIG:
                if( debugLevel == OUTPUT_RAW)
                {
                    printf("CONFIG RAW   : ");
                    for(idx = 0; idx < yyleng; idx++)
                    {
                        printf("%.2X",(unsigned char)yytext[idx]);
                    }
                    printf("\n");
                }
                handleConfigPacket((unsigned char *)yytext, yyleng, &currentConfigPacket);
                currentConfigPacket.isValid = 1;
                storeConfigPacket(connection, &currentConfigPacket);
                if( debugLevel == OUTPUT_DECODED )
                {
                    logConfigPacket(&currentConfigPacket);
                }
                break;

            case TOKEN_UNKNOWN:
                printf( "Unrecognized character in stream: %.2X\n", (unsigned char)yytext[0] );
                break;

            default:
                printf("Error tokenizer ... this is bad!\n");
                break;
        }
    }

    // 
    // Wait totdat heartbeat thread is gestopt
    //
    hbdone = 1;
    pthread_join(hbThread, NULL);

    // Capture session stop time
    //clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &sessionStop);

    gettimeofday(&sessionStop, NULL);
}


//
//
//
int main(int argc, char *argv[])
{   
    int socketfd;
    struct sockaddr_in serverAddress;
    struct sockaddr_in clientAddress;
    int portnr = SERVER_PORT;

    //
    // Handle options
    //
    int option = 0;
    while( (option = getopt(argc, argv,"hd:p:")) != -1 )
    {
        switch(option)
        {
            case 'd': 
                debugLevel = atoi(optarg);
                if( debugLevel > 3)
                {
                    printf("usage: %s [-d {0,1,2,3}] [-p port]\n", argv[0]);
                    exit(0);
                }
                break;

            case 'p':
                portnr = atoi(optarg);
                break;

            case 'h':
                printf("usage: %s [-d {0,1,2,3}] [-p port]\n", argv[0]);
                exit(0);
        }
    }
    printf("listening @port:%d, debug:%d \n",portnr, debugLevel);

    // 
    // Create socket
    //
    socketfd = socket(PF_INET, SOCK_STREAM, 0);
    // 
    bzero(&serverAddress, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(portnr);
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    if( bind(socketfd, (struct sockaddr*)&serverAddress, sizeof(serverAddress)) != 0 )
    {
        perror("bind() failed");
    }

    // Listen
    listen(socketfd, 75);

    struct sigaction sa;
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = signalHandler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    if( sigaction(SIGCHLD, &sa, NULL) < 0 )
    {
        perror("sigaction() failed");
    }

    // Log to syslog
    openlog("fts", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL0);

    for (;;)
    {
        socklen_t socketsize = sizeof(struct sockaddr_in);
        int client = accept(socketfd, (struct sockaddr *)&clientAddress, &socketsize);
        int pid;

        if ( client > 0 )
        {
            if ( 0 == (pid = fork()) )
            {
                // Child process
                close(socketfd);

                // 
                fprintf(stdout,"Incomming connection from %s on port %d\n", 
                inet_ntoa(clientAddress.sin_addr), 
                htons(clientAddress.sin_port));

                // 
                handleConnection(client);
                
                //
                //struct timespec duration = timespecdiff(sessionStart, sessionStop);
                
                long sessionDuration = (((sessionStop.tv_sec - sessionStart.tv_sec) * 1000000L) + 
                    (sessionStop.tv_usec - sessionStart.tv_usec));

                fprintf(stdout,"Closing connection from %s on port %d. "
                    "Session duration: %ld us\n", 
                    inet_ntoa(clientAddress.sin_addr), 
                    htons(clientAddress.sin_port),
                    sessionDuration);

                    //(duration.tv_sec + (duration.tv_nsec / 1000000000.0)));

                // close socket and bye
                close(client);

                // close log
                closelog();

                // Client exit's
                exit(0);
            }
            else
            {
                //parent process
                close(client);
            }
        }
        else
        {
            perror("accept() failed");
            return 0;
        }
    }

    close(socketfd);

    closelog();
    
    return 0;
}


//
// Spit de data in big switch-case. Geen memcpy omdat dit 2x een loop is.
//
void handleConfigPacket(unsigned char *data, int leng, CONFIGPACKET_TYPE_0X11_STRUCT *configPacket)
{
    int idx = 0;
    int dbyteCnt = 0;
    unsigned char dbyte;
    while(idx < leng)
    {
        if( (0x10 == data[idx]) && (idx != leng) )
        {
            if (0x82 == data[idx+1]) dbyte = 0x02;
            if (0x83 == data[idx+1]) dbyte = 0x03;
            if (0x90 == data[idx+1]) dbyte = 0x10;
            idx+=2;
        }
        else
        {
            dbyte = data[idx++];
        }
        
        // Handle received byte
        switch( dbyteCnt )
        {
            // 
            case 0: break; // 0x02
            case 1: break; // 0x44
            case 2: break; // 0x11

            // 
            // Pressure calibration sensor 0
            case 3: configPacket->presureCalVals[0].offset = dbyte; break;
            case 4: configPacket->presureCalVals[0].gain = dbyte; break;
            // 
            // Pressure calibration sensor 1
            case 5: configPacket->presureCalVals[1].offset = dbyte; break;
            case 6: configPacket->presureCalVals[1].gain = dbyte; break;
            // 
            // Pressure calibration sensor 2
            case 7: configPacket->presureCalVals[2].offset = dbyte; break;
            case 8: configPacket->presureCalVals[2].gain = dbyte; break;
            // 
            // Pressure calibration sensor 3
            case 9: configPacket->presureCalVals[3].offset = dbyte; break;
            case 10: configPacket->presureCalVals[3].gain = dbyte; break;
            // 
            // Pressure calibration sensor 4
            case 11: configPacket->presureCalVals[4].offset = dbyte; break;
            case 12: configPacket->presureCalVals[4].gain = dbyte; break;
            // 
            // Pressure calibration sensor 5
            case 13: configPacket->presureCalVals[5].offset = dbyte; break;
            case 14: configPacket->presureCalVals[5].gain = dbyte; break;
            // 
            // Pressure calibration sensor 6
            case 15: configPacket->presureCalVals[6].offset = dbyte; break;
            case 16: configPacket->presureCalVals[6].gain = dbyte; break;
            // 
            // Pressure calibration sensor 7
            case 17: configPacket->presureCalVals[7].offset = dbyte; break;
            case 18: configPacket->presureCalVals[7].gain = dbyte; break;
            // 
            // Pressure calibration sensor 8
            case 19: configPacket->presureCalVals[8].offset = dbyte; break;
            case 20: configPacket->presureCalVals[8].gain = dbyte; break;
            // 
            // Pressure calibration sensor 9
            case 21: configPacket->presureCalVals[9].offset = dbyte; break;
            case 22: configPacket->presureCalVals[9].gain = dbyte; break;
            // 
            // Pressure calibration sensor 10
            case 23: configPacket->presureCalVals[10].offset = dbyte; break;
            case 24: configPacket->presureCalVals[10].gain = dbyte; break;
            // 
            // Pressure calibration sensor 11
            case 25: configPacket->presureCalVals[11].offset = dbyte; break;
            case 26: configPacket->presureCalVals[11].gain = dbyte; break;
            // 
            // Pressure calibration sensor 12
            case 27: configPacket->presureCalVals[12].offset = dbyte; break;
            case 28: configPacket->presureCalVals[12].gain = dbyte; break;
            // 
            // Pressure calibration sensor 13
            case 29: configPacket->presureCalVals[13].offset = dbyte; break;
            case 30: configPacket->presureCalVals[13].gain = dbyte; break;
            // 
            // Pressure calibration sensor 14
            case 31: configPacket->presureCalVals[14].offset = dbyte; break;
            case 32: configPacket->presureCalVals[14].gain = dbyte; break;
            // 
            // Pressure calibration sensor 15
            case 33: configPacket->presureCalVals[15].offset = dbyte; break;
            case 34: configPacket->presureCalVals[15].gain = dbyte; break;
            // 
            // Pressure calibration sensor 16
            case 35: configPacket->presureCalVals[16].offset = dbyte; break;
            case 36: configPacket->presureCalVals[16].gain = dbyte; break;
            // 
            // Pressure calibration sensor 17
            case 37: configPacket->presureCalVals[17].offset = dbyte; break;
            case 38: configPacket->presureCalVals[17].gain = dbyte; break;
            // 
            // Pressure calibration sensor 18
            case 39: configPacket->presureCalVals[18].offset = dbyte; break;
            case 40: configPacket->presureCalVals[18].gain = dbyte; break;
            // 
            // Pressure calibration sensor 19
            case 41: configPacket->presureCalVals[19].offset = dbyte; break;
            case 42: configPacket->presureCalVals[19].gain = dbyte; break;

            // Accelerometer 0
            case 43: configPacket->acceleroCalVals[0].offsetX = dbyte; break;
            case 44: configPacket->acceleroCalVals[0].offsetY = dbyte; break;
            case 45: configPacket->acceleroCalVals[0].offsetZ = dbyte; break;

            // Accelerometer 1
            case 46: configPacket->acceleroCalVals[1].offsetX = dbyte; break;
            case 47: configPacket->acceleroCalVals[1].offsetY = dbyte; break;
            case 48: configPacket->acceleroCalVals[1].offsetZ = dbyte; break;

            // Temperature
            case 49: configPacket->tempCalVals.offset = dbyte; break;
            case 50: configPacket->tempCalVals.gain = dbyte; break;
          
            // Serial number (UID)
            case 51: configPacket->serialNr  = (dbyte<<24); break;
            case 52: configPacket->serialNr |= (dbyte<<16); break;
            case 53: configPacket->serialNr |= (dbyte<<8); break;
            case 54: configPacket->serialNr |= (dbyte<<0); break;
            
            // Pad type
            case 55: configPacket->padType = dbyte; break;

            // CRC
            case 56: configPacket->crc = dbyte; break;

            case 57: break; //0x03 [STX]

            default:
                    break;

        }
        dbyteCnt++;
    }
}

//
// Spit de data in big switch-case. Geen memcpy omdat dit 2x een loop is.
//
void handlePowerPacket(unsigned char *data, int leng, POWERPACKET_STRUCT *powerPacket)
{
    int idx = 0;
    int dbyteCnt = 0;
    unsigned char dbyte;
    while(idx < leng)
    {
        if( (0x10 == data[idx]) && (idx != leng) )
        {
            if (0x82 == data[idx+1]) dbyte = 0x02;
            if (0x83 == data[idx+1]) dbyte = 0x03;
            if (0x90 == data[idx+1]) dbyte = 0x10;
            idx+=2;
        }
        else
        {
            dbyte = data[idx++];
        }

        // Handle received byte
        switch( dbyteCnt )
        {
            // 
            case 0: break;
            case 1: break;

            // batteryLevel
            case 2: powerPacket->batteryLevel = dbyte; break;
            
            // is on charger
            case 3: powerPacket->onCharger = dbyte; break;
            
            case 4: powerPacket->crc = dbyte;
                     break;

            case 5: break;  // 0x03 [STX]


            default:
                    break;

        }
        dbyteCnt++;
    }
}


//
// Spit de data in big switch-case. Geen memcpy omdat dit 2x een loop is.
//
void handleDataPacket(unsigned char *data, int leng, DATAPACKET_STRUCT *dataPacket)
{
    int idx = 0;
    int dbyteCnt = 0;
    unsigned char dbyte;
    while(idx < leng)
    {
        if( (0x10 == data[idx]) && (idx != leng) )
        {
            if (0x82 == data[idx+1]) dbyte = 0x02;
            if (0x83 == data[idx+1]) dbyte = 0x03;
            if (0x90 == data[idx+1]) dbyte = 0x10;
            idx+=2;
        }
        else
        {
            dbyte = data[idx++];
        }

        // Handle received bytes
        switch( dbyteCnt )
        {
            // 
            case 0: break;
            case 1: break;

            // Packet counter (3 x 1 bytes)
            case 2: dataPacket->packetCounter = (dbyte<<16); break;
            case 3: dataPacket->packetCounter |= (dbyte<<8); break;
            case 4: dataPacket->packetCounter |= dbyte; break;

            // 
            // Pressure sensor 0
            case 5: dataPacket->pressure[0] = (dbyte << 8); break;
            case 6: dataPacket->pressure[0] |= dbyte; break;
            
            // Pressure sensor 1 
            case 7: dataPacket->pressure[1] = (dbyte << 8); break;
            case 8: dataPacket->pressure[1] |= dbyte; break;

            // Pressure sensor 2
            case 9: dataPacket->pressure[2] = (dbyte << 8); break;
            case 10: dataPacket->pressure[2] |= dbyte; break;
            
            // Pressure sensor 3
            case 11: dataPacket->pressure[3] = (dbyte << 8); break;
            case 12: dataPacket->pressure[3] |= dbyte; break;
           
            // Pressure sensor 4
            case 13: dataPacket->pressure[4] = (dbyte << 8); break;
            case 14: dataPacket->pressure[4] |= dbyte; break;
           
            // Pressure sensor 5
            case 15: dataPacket->pressure[5] = (dbyte << 8); break;
            case 16: dataPacket->pressure[5] |= dbyte; break;
           
            // Pressure sensor 6
            case 17: dataPacket->pressure[6] = (dbyte << 8); break;
            case 18: dataPacket->pressure[6] |= dbyte; break;
           
            // Pressure sensor 7
            case 19: dataPacket->pressure[7] = (dbyte << 8); break;
            case 20: dataPacket->pressure[7] |= dbyte; break;
           
            // Pressure sensor 8
            case 21: dataPacket->pressure[8] = (dbyte << 8); break;
            case 22: dataPacket->pressure[8] |= dbyte; break;
           
            // Pressure sensor 9
            case 23: dataPacket->pressure[9] = (dbyte << 8); break;
            case 24: dataPacket->pressure[9] |= dbyte; break;
           
            // Pressure sensor 10
            case 25: dataPacket->pressure[10] = (dbyte << 8); break;
            case 26: dataPacket->pressure[10] |= dbyte; break;
           
            // Pressure sensor 11
            case 27: dataPacket->pressure[11] = (dbyte << 8); break;
            case 28: dataPacket->pressure[11] |= dbyte; break;
           
            // Pressure sensor 12
            case 29: dataPacket->pressure[12] = (dbyte << 8); break;
            case 30: dataPacket->pressure[12] |= dbyte; break;
           
            // Pressure sensor 13
            case 31: dataPacket->pressure[13] = (dbyte << 8); break;
            case 32: dataPacket->pressure[13] |= dbyte; break;
           
            // Pressure sensor 14
            case 33: dataPacket->pressure[14] = (dbyte << 8); break;
            case 34: dataPacket->pressure[14] |= dbyte; break;
           
            // Pressure sensor 15
            case 35: dataPacket->pressure[15] = (dbyte << 8); break;
            case 36: dataPacket->pressure[15] |= dbyte; break;
           
            // Pressure sensor 16
            case 37: dataPacket->pressure[16] = (dbyte << 8); break;
            case 38: dataPacket->pressure[16] |= dbyte; break;
           
            // Pressure sensor 17
            case 39: dataPacket->pressure[17] = (dbyte << 8); break;
            case 40: dataPacket->pressure[17] |= dbyte; break;
           
            // Pressure sensor 18
            case 41: dataPacket->pressure[18] = (dbyte << 8); break;
            case 42: dataPacket->pressure[18] |= dbyte; break;
           
            // Pressure sensor 19
            case 43: dataPacket->pressure[19] = (dbyte << 8); break;
            case 44: dataPacket->pressure[19] |= dbyte; break;
           
            // Accelerometer 0 - x
            case 45: dataPacket->accelero[0].x = (dbyte << 8); break;
            case 46: dataPacket->accelero[0].x |= dbyte; break;
           
            // Accelerometer 0 - y
            case 47: dataPacket->accelero[0].y = (dbyte << 8); break;
            case 48: dataPacket->accelero[0].y |= dbyte; break;
            
            // Accelerometer 0 - z
            case 49: dataPacket->accelero[0].z = (dbyte << 8); break;
            case 50: dataPacket->accelero[0].z |= dbyte; break;
            
            // Accelerometer 1 - x
            case 51: dataPacket->accelero[1].x = (dbyte << 8); break;
            case 52: dataPacket->accelero[1].x |= dbyte; break;
           
            // Accelerometer 1 - y
            case 53: dataPacket->accelero[1].y = (dbyte << 8); break;
            case 54: dataPacket->accelero[1].y |= dbyte; break;
            
            // Accelerometer 1 - z
            case 55: dataPacket->accelero[1].z = (dbyte << 8); break;
            case 56: dataPacket->accelero[1].z |= dbyte; break;

            // temperature
            case 57: dataPacket->temperature = (dbyte << 8); break;
            case 58: dataPacket->temperature |= dbyte; break;

            case 59: dataPacket->crc = dbyte;
                     break;
            
            case 60: break;


            default:
                    break;

        }
        dbyteCnt++;
    }
}

//
// 
//
MYSQL *connectToDatabase(void)
{
    MYSQL *connection;
    connection = mysql_init(NULL);
    
    printf("mysql_get_client_info()= %s\n", mysql_get_client_info());
    // connect to the database
    if (mysql_real_connect(connection, 
        DATABASE_SERVER, 
        DATABASE_USER, 
        DATABASE_PASSWORD, 
        DATABASE_SCHEMA, 
        0, NULL, 0)) 
    {
        return connection;
    }
    else 
    {
        printf("mysql_error()=%s\n", mysql_error(connection));
        return NULL;
    }
}

// 
//
//
void storePowerPacket(MYSQL *connection, POWERPACKET_STRUCT *powerPacket)
{
    char query[4096];

    // Create the query to add the data to the database
    sprintf(query,
        "INSERT INTO powerinfo (created, batteryLevel, onCharger, sessionUuid)"
        "VALUES(%s, %d, %d, \"%s\");",
        "now()",
        powerPacket->batteryLevel,
        powerPacket->onCharger,
        sessionUuid
    ); 

    if( debugLevel == OUTPUT_SQL )
       fprintf(stdout, "[%d]%s\n", strlen(query),query);
  
    // Perform the query
    if(mysql_query(connection, query)) 
    {
        printf("MySQL Error: %s\n", mysql_error(connection));
    }
}

//
// 
//
void storeConfigPacket(MYSQL *connection, CONFIGPACKET_TYPE_0X11_STRUCT *configPacket)
{
    char query[4096];

    // Create the query to add the data to the database
    sprintf(query,
        "INSERT INTO footpod (serialNumber, padType, created, description, sessionUuid)"
        "VALUES(%lu, %s, %s, %lu, \"%s\");",
        configPacket->serialNr,
        ((0 == configPacket->padType)?"\"left\"":"\"right\""),
        "now()",
        configPacket->serialNr,
        sessionUuid
    ); 

    if( debugLevel == OUTPUT_SQL )
       fprintf(stdout, "[%d]%s\n", strlen(query),query);
  
    // Perform the query
    if(mysql_query(connection, query)) 
    {
        printf("MySQL Error: %s\n", mysql_error(connection));
    }
}


void storePressurePacket(MYSQL *connection, DATAPACKET_STRUCT *dataPacket);
void storeAccelerometerPacket(MYSQL *connection, DATAPACKET_STRUCT *dataPacket, ACCELERO_SELECT_ENUM asel);
void storeTemperaturePacket(MYSQL *connection, DATAPACKET_STRUCT *dataPacket);

//
// 
//
void storeDataPacket(MYSQL *connection, DATAPACKET_STRUCT *dataPacket)
{
    //Start transaction
    if(mysql_query(connection, "START TRANSACTION")) 
    {
        fprintf(stderr, "MySQL Error: %s\n", mysql_error(connection));
    }

    // Store dataPacket in db
    storePressurePacket(connection, dataPacket);
    storeAccelerometerPacket(connection, dataPacket, TOE);
    storeAccelerometerPacket(connection, dataPacket, HEEL);
    storeTemperaturePacket(connection, dataPacket);

    // Commit transaction
    if(mysql_query(connection, "COMMIT")) 
    {
        fprintf(stderr,"MySQL Error: %s\n", mysql_error(connection));
    }
}


//
// 
//
void storePressurePacket(MYSQL *connection, DATAPACKET_STRUCT *dataPacket)
{
    char query[4096];

    // Create the query to add the data to the database
    sprintf(query,
        "INSERT INTO pressure (created, packetCounter,"
            "sensor0, sensor1, sensor2, sensor3, sensor4," 
            "sensor5, sensor6, sensor7, sensor8, sensor9,"
            "sensor10, sensor11, sensor12, sensor13, sensor14,"
            "sensor15, sensor16, sensor17, sensor18, sensor19,"
            "sessionUuid)"
        "VALUES(%s, %lu," 
            "%d, %d, %d, %d, %d," 
            "%d, %d, %d, %d, %d," 
            "%d, %d, %d, %d, %d," 
            "%d, %d, %d, %d, %d," 
            "\"%s\");",  
            "now()",    
            dataPacket->packetCounter,
            dataPacket->pressure[0], dataPacket->pressure[1],dataPacket->pressure[2], 
            dataPacket->pressure[3], dataPacket->pressure[4],dataPacket->pressure[5], 
            dataPacket->pressure[6], dataPacket->pressure[7],dataPacket->pressure[8], 
            dataPacket->pressure[9], dataPacket->pressure[10],dataPacket->pressure[11], 
            dataPacket->pressure[12], dataPacket->pressure[13],dataPacket->pressure[14], 
            dataPacket->pressure[15], dataPacket->pressure[16],dataPacket->pressure[17],
            dataPacket->pressure[17], dataPacket->pressure[19],
            sessionUuid); 

    if( debugLevel == OUTPUT_SQL )
       fprintf(stdout, "[%d]%s\n", strlen(query),query);
  
    // Perform the query
    if(mysql_query(connection, query)) 
    {
        printf("MySQL Error: %s\n", mysql_error(connection));
    }
}

void storeAccelerometerPacket(MYSQL *connection, DATAPACKET_STRUCT *dataPacket, ACCELERO_SELECT_ENUM asel)
{
    char query[4096];

    // Create the query to add the data to the database
    sprintf(query,
        "INSERT INTO %s(created, packetCounter,"
            "x,y,z,sessionUuid)"
        "VALUES(%s, %lu," 
            "%d,%d,%d,\"%s\");",     
            ((asel==0)?"accelerometer_heel":"accelerometer_toe"),
            "now()", 
            dataPacket->packetCounter,
            dataPacket->accelero[asel].x, 
            dataPacket->accelero[asel].y,
            dataPacket->accelero[asel].z, 
            sessionUuid); 

    if( debugLevel == OUTPUT_SQL )
       fprintf(stdout, "[%d]%s\n", strlen(query),query);
  
    // Perform the query
    if(mysql_query(connection, query)) 
    {
        fprintf(stdout, "MySQL Error: %s\n", mysql_error(connection));
    }
}

void storeTemperaturePacket(MYSQL *connection, DATAPACKET_STRUCT *dataPacket)
{
    char query[4096];

    // Create the query to add the data to the database
    sprintf(query,
        "INSERT INTO temperature (created," 
            "packetCounter,"
            "temperature,"
            "sessionUuid)"
        "VALUES(%s, %lu," 
            "%d,\"%s\");",  
            "now()",    
            dataPacket->packetCounter,
            dataPacket->temperature,  
            sessionUuid); 

    if( debugLevel == OUTPUT_SQL )
       fprintf(stdout, "[%d]%s\n", strlen(query),query);
  
    // Perform the query
    if(mysql_query(connection, query)) 
    {
        printf("MySQL Error: %s\n", mysql_error(connection));
    }
}

//
// http://www.guyrutenberg.com/2007/09/22/profiling-code-using-clock_gettime/
//
struct timespec timespecdiff(struct timespec start, struct timespec end)
{
    struct timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}

//
//
//
void genuuid(char *uuid)
{
    srand(time(NULL));

    sprintf(uuid, "%x%x-%x-%x-%x-%x%x%x", 
        rand(), rand(),                 // Generates a 64-bit Hex number
        rand(),                         // Generates a 32-bit Hex number
        ((rand() & 0x0fff) | 0x4000),   // Generates a 32-bit Hex number of the form 4xxx (4 indicates the UUID version)
        rand() % 0x3fff + 0x8000,       // Generates a 32-bit Hex number in the range [0x8000, 0xbfff]
        rand(), rand(), rand());        // Generates a 96-bit Hex number
}

//
//
//
void logConfigPacket(CONFIGPACKET_TYPE_0X11_STRUCT *configPacket)
{   
    int idx;

    printf("\n-------------------- CONFIG --------------------\n");
    printf("Serial Number: 0x%.4lX\n", configPacket->serialNr);
    //printf("Serial Number: 0x%.2X%.2X%.2X%.2X\n", 
    //    configPacket->serialNr[0],
    //    configPacket->serialNr[1],
    //    configPacket->serialNr[2],
    //    configPacket->serialNr[3]);
    
    
    // Pressure
    for(idx = 0; idx < 20; idx++ )
    {
        printf("pCal_%.2d = {offset=%.2d,gain=%.2d}\n", 
            idx, 
            configPacket->presureCalVals[idx].offset,
            configPacket->presureCalVals[idx].gain);
    }

    // Accelero 0 + 1
    for(idx = 0; idx < 2; idx++ )
    {
        printf("acceleroCal_%.2d = {offsetX=%.2d,offsetY=%.2d,offsetZ=%.2d}\n", 
            idx, 
            configPacket->acceleroCalVals[idx].offsetX,
            configPacket->acceleroCalVals[idx].offsetY,
            configPacket->acceleroCalVals[idx].offsetZ);
    }

    // Temperature
    printf("tempCal = {offsetX=%.2d,gain=%.2d}\n", 
            configPacket->tempCalVals.offset,
            configPacket->tempCalVals.gain);

    // Pad type    
    if( 0 == configPacket->padType )
    {
        printf("padtype =  LEFT pad\n");
    }
    else
    {
        printf("padtype =  RIGHT pad\n"); 
    }

    printf("CRC: 0x%.2X [not checked, TCP is already reliable]\n", 
        configPacket->crc);
    printf("----------------------------------------------\n");
}

//
//
//
void logPowerPacket(POWERPACKET_STRUCT *powerPacket)
{  
    printf("\n------------------- POWER --------------------\n");
    printf("Battery Level: %d [%%] (0x%.2X)\n", powerPacket->batteryLevel, 
        powerPacket->batteryLevel);
    if( 0 == powerPacket->onCharger )
        printf("Is on charger: FALSE [BOOL] (0x%.2X)\n",powerPacket->onCharger);
    else 
        printf("Is on charger: TRUE [BOOL] (0x%.2X)\n",powerPacket->onCharger);
    printf("CRC: 0x%.2X [not checked, TCP is already reliable]\n", powerPacket->crc);
    printf("----------------------------------------------\n");
}

//
//
//
void logDataPacket(DATAPACKET_STRUCT *dataPacket)
{  
    int idx;

    printf("\n-------------------- DATA --------------------\n");
    printf("Packet count:\t%lu\n", dataPacket->packetCounter);
    for(idx = 0; idx < 20; idx++ )
    {
        printf("Pressure %.2d : %d [?] (0x%.4X)\n", idx, 
            dataPacket->pressure[idx], dataPacket->pressure[idx]);
    }
    for(idx = 0; idx < 2; idx++ )
    {
        printf("Accelero %d X : %d [mG] (0x%.4X)\n", idx, 
            dataPacket->accelero[idx].x, dataPacket->accelero[idx].x);
        printf("Accelero %d Y : %d [mG] (0x%.4X)\n", idx, 
            dataPacket->accelero[idx].y, dataPacket->accelero[idx].y);
        printf("Accelero %d Z : %d [mG] (0x%.4X)\n", idx, 
            dataPacket->accelero[idx].z, dataPacket->accelero[idx].z);
    }
    printf("Temp : %d [C] (0x%.4X)\n", dataPacket->temperature, 
        dataPacket->temperature);
    printf("CRC: 0x%.2X [not checked, TCP is already reliable]\n", dataPacket->crc);
    printf("----------------------------------------------\n");
}


 
