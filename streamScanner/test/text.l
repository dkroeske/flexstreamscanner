%{

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define BUFFERSIZE 26
                       /* 01234567890123456789012345 */
char cbuf0[BUFFERSIZE] = "Hello everybody, lex is su";  // Warning, no '\0'
char cbuf1[BUFFERSIZE] = "per cool. Thanks!         ";
char recvBuffer[BUFFERSIZE];

int packetCnt = 0;
int bufferIndex = 0;
int bufferSelector = 0;
int nrPackets = 0;
int done = 0;

void inputToFlex(char *buf, long unsigned *result, int max_size);


#undef YY_INPUT
#define YY_INPUT(buf, result, max_size) inputToFlex(buf, &result, max_size)


%}

%option nounput
%option noinput

%%

"super"                 { printf( "Hit on: \"%s\"\n", yytext );; }
.                       { printf( "%c", yytext[0] );}

%%

//
//
//
int yywrap()
{
    printf(">> yywrap()\n");    
    return(1); 
}

//
//
//
void inputToFlex(char *buf, long unsigned *result, int max_size)
{
    
    if( bufferIndex == BUFFERSIZE)
    {
        if( !done )
        {
            memcpy(recvBuffer, cbuf1, BUFFERSIZE);
            bufferIndex = 0;
            int c = recvBuffer[bufferIndex++];
            buf[0] = c;
            done = 1;
            *result = 1;
        }
        else
        {
            *result = YY_NULL;
        }
    }
    else
    {
        int c = recvBuffer[bufferIndex++];
        buf[0] = c;
        *result = 1;
    }
 }

//
//
//
int main(void)
{
    printf("Lenght: %d\n", (int)sizeof(recvBuffer)) ;

    memcpy(recvBuffer, cbuf0, BUFFERSIZE);
    bufferSelector = 1;

    //
    packetCnt = 0;

    //
    yylex();

    return 0;
}
