%{

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define BUFFERSIZE 26
                       /*   0123456789012345678901234 */
char cbuf1[BUFFERSIZE] = "Hello everybody, lex is su";  // Warning, no '\0'
char cbuf2[BUFFERSIZE] = "per cool. Thanks!         ";
char recvBuffer[BUFFERSIZE];

int packetCnt = 0;

YY_BUFFER_STATE bufferState1, bufferState2;

%}

%option nounput
%option noinput

%%

"super"                 { ECHO; }
.                       { printf( "%c", yytext[0] );}

%%

int yywrap()
{
 
    int retval = 1;   

    printf(">> yywrap()\n");

    if( packetCnt <= 0 )    // Stop after 2
    {
        // Copy cbuf2 into recvBuffer
        memcpy(recvBuffer, cbuf2, BUFFERSIZE);
        
        //
        yyrestart(NULL); // ?? has no effect

        // Feed it to flex
        bufferState2 = yy_scan_bytes(recvBuffer, BUFFERSIZE); 
        
        //
        packetCnt++;
        
        // Tell flex to resume scanning
        retval = 0;   
    }
    
    return(retval); 
}

int main(void)
{
    printf("Lenght: %d\n", (int)sizeof(recvBuffer)) ;

    // Copy cbuf1 into recvBuffer
    memcpy(recvBuffer, cbuf1, BUFFERSIZE);

    //
    packetCnt = 0;

    //
    bufferState1 = yy_scan_bytes(recvBuffer, BUFFERSIZE);

    //
    yylex();

    yy_delete_buffer(bufferState1);
    yy_delete_buffer(bufferState2);

    return 0;
}
