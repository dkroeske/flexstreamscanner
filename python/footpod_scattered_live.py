import footpod_db as fpdb

import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation

# Gemiddelde van <limit> aantal samples
limit = 10
uuid = ""

# db host IP
fpinfo = fpdb.FootpodDB(host = "192.168.2.138")

#
# Hardcoded positie van alle sensoren op de LINKER zool tov background
# image 
#
sensorMap = [(147,247), (161,57),  (95,516), (129,400),  (163,132),
             (178,191), (136,467), (128,313),  (26,188),  (47,135),
             (43,300),  (48,468),  (71,55),   (51,238),  (54,394),
             (91,440),  (125,20),  (127,115), (117,192), (98,259)]

def main():

    # Kleur info
    data = np.zeros(20)
    
    # Plot achtergrond plaatje
    fig = plt.figure()
    image = plt.imread("footpod_left.png")
    implot = plt.imshow(image)
    
    # Plot alle druk punten in de scatter
    x = []
    y = []
    for coord in sensorMap:
        x.append(coord[0])
        y.append(coord[1])
    
    # Colormap
    cm = plt.get_cmap('jet')

    # scatter plt
    scat = plt.scatter(x=x, y=y, vmin=0.0, vmax=1.0, c=[0.1 for i in range(20)], s=350, cmap=cm)
 	
 	# animatie properties
    ani = animation.FuncAnimation(fig, update, fargs=("db", scat))

    # plot scatter en colorbar
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title("Footpod pressure")
    plt.colorbar(scat)
    plt.show()

def update(i, database, scat):
    global uuid
    session = fpinfo.mostRecentSession()
    if(uuid != session.uuid):
        plt.suptitle("Session: " + session.uuid + " (" + str(session.date) + ")")
        uuid = session.uuid

    data = fpinfo.pressures(uuid = uuid, limit = limit)
    
    scat.set_array(data)
    return scat

# Python entry point
if __name__ == "__main__":
    main()
