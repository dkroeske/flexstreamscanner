#!/usr/bin/python
import MySQLdb
from pylab import *

print "Connecting to footpod database"
db = MySQLdb.connect(host="10.211.55.6", user="footpod", passwd="footpod", db="FootpodAnalyser")
cur = db.cursor()

#
#
query = """select packetCounter, sensor0, sensor1 from pressure where 
			sessionUuid = '2b4fa098b5db898-1d57bf3e-4e7f-a6'"""
print "query:{:s}".format(query)
cur.execute(query)
#for row in cur.fetchall():
#	print "UUID:{:d}".format(row[0])
count = cur.rowcount
row = cur.fetchall()

#
x = np.linspace(1, count, count, endpoint=True)
y = []
for value in row:
	y.append(value[0])
plot(x,y, color="red", linewidth=2.0, linestyle="-", label = "packetloss")

xlim(1, max(y)+100)
#xticks(np.linspace(1, count, 10, endpoint=True))
ylim(1, max(y)+100)
#yticks(np.linspace(1, count, 10, endpoint=True))

legend(loc='upper left');

show()

