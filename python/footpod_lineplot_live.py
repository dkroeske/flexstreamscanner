# database connection
import footpod_db as fpdb
import sys, getopt

# from pylab import *
import numpy as np
import matplotlib as mpl
from matplotlib import pyplot 
from matplotlib import animation


dbhost = "192.168.2.138"
nrSamples = 250
nrLines = 20
lines = []
x = []
y = []
uuid = ""

# db host IP
fpinfo = fpdb.FootpodDB(host = dbhost)
figure = pyplot.figure()

# Called once by animation
def init():
	# Create array of lines
	ax = pyplot.axes(xlim=(0,nrSamples), ylim=(0,0xFFFF))
	
	for n in range(0,nrLines):
		line = ax.plot([], [], lw=2)[0];
		line.set_label("Pressure :"+str(n))
		lines.append(line)
	
	pyplot.yticks([0, 1*0xFFFF/4, 2*0xFFFF/4, 3*0xFFFF/4, 4*0xFFFF/4],
		['0x0000', '0x3FFF', '0x7FFF', '0xBFFF', '0xFFFF'])
	pyplot.legend(loc='upper left', fontsize='small', ncol=nrLines/10, title="Pressure Sensors")
	pyplot.xlabel('last n sensorsamples')
	pyplot.ylabel('sensor value (raw hex)')
	pyplot.title('Uncalibrated Pressure Sensor data')
	pyplot.grid(True)

	# Init each line
	for line in lines:
		line.set_data([], [])
	return lines

# Called periodically by animation
def animate(i):
	global x,y, uuid
	x = np.linspace(0,nrSamples-1, num = nrSamples)

	session = fpinfo.mostRecentSession()
	if(uuid != session.uuid):
		pyplot.suptitle("Session: " + session.uuid + " (" + str(session.date) + ")")
		uuid = session.uuid

	# Returns for all channels (0-19) last nrSampels (if available)
	data = fpinfo.pressureSamples(uuid = uuid, nrSamples = nrSamples)

	if( len(data) >= nrSamples ):
		for n in range(0, nrLines):
			y = []
			for value in data:
				y.append(value[n])
			lines[n].set_data(x,y)

	return lines

# Plot de grafiek
anim = animation.FuncAnimation(figure, animate, init_func=init,
	frames=nrSamples, interval=1000*(1.0/nrSamples), blit=False)

#anim.save(uuid+".mp4", fps=30, extra_args=['-vcodec', 'libx264'])

pyplot.show()


