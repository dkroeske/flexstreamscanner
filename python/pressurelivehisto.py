import MySQLdb
import sys, getopt

#from pylab import *
import numpy as np
from numpy.random import randn
import matplotlib as mpl
from matplotlib import pyplot 
from matplotlib import animation

sensorid = "0"
dbipaddr = "192.168.2.172"
limit = 200
#
#
def main(argv):
	global sensorid, dpipaddr, limit
	try:
		opts, args = getopt.getopt(argv,"hps:ip:l:",[])
	except getopt.GetoptError:
		print "pressurelive.py -ps <pressure_sensor> -ip <db ip_adress> -l <limit>"
		sys.exit(2)
	for opt, arg in opts:
		if opt == "-h":
			print "pressurelive.py -ps <pressure_sensor> -ip <db ip_adress> -l <limit>"
			sys.exit()
		elif opt in ("-ps"):
			sensorid = arg
			print arg
		elif opt in ("-l"):
			limit = arg
		elif opt in ("-ip"):
			dbipaddr = arg
if __name__ == "__main__":
	main(sys.argv[1:])

print "Query pressurese sensor:" + sensorid +" @db:" + dbipaddr
print "matplotlib.version: " + mpl.__version__ + " (" + mpl.__file__ + ")"
print "Connecting to footpod database"
db = MySQLdb.connect(host="192.168.2.172", user="footpod", passwd="footpod", db="FootpodAnalyser")
db.autocommit(True)
cur = db.cursor()

#
# Get latest session
#
query = """SELECT sessionUuid, created FROM footpod ORDER BY created DESC LIMIT 1"""
cur.execute(query)
row = cur.fetchall()
for value in row:
	uuid = value[0]
	date = value[1]
print "Unique session ID: " + uuid
print "Recorded on:" + str(date)

#
# Do query string and print
query = "select sensor0, packetCounter from pressure where sessionUuid = \'" + uuid +"\' ORDER BY pressure.packetCounter DESC LIMIT " + str(limit)
print "query string: " + query

#figure = pyplot.figure()
# ax = pyplot.axes(xlim=(0,limit), ylim=(0,0xFFFF))
# pyplot.yticks([0, 1*0xFFFF/4, 2*0xFFFF/4, 3*0xFFFF/4, 4*0xFFFF/4],
# 	['0x0000', '0x3FFF', '0x7FFF', '0xBFFF', '0xFFFF'])
# pyplot.legend(loc='upper left')
# pyplot.xlabel('last n sensorsamples')
# pyplot.ylabel('sensor value (raw hex)')
# pyplot.title("Uncalibrated Pressure Sensor(" + str(0) + ") Data\n(session ID:\'" + uuid + "\' @" + str(date)+")")

#line, = ax.plot([], [], lw=2);

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

fig = plt.figure()

def f(x, y):
    return x+0*y #np.sin(x) + np.cos(y) 

x = np.linspace(0, 2*np.pi, 120)
y = np.linspace(0, 2*np.pi, 120).reshape(-1,1)

im = plt.imshow(f(x, y), cmap=plt.get_cmap('jet'))
im.set_array(f(x,y))
def updatefig(*args):
    global x,y
    x += np.pi / 15.
    y += np.pi / 20.
    im.set_array(f(x,y))
    return im,

#ani = animation.FuncAnimation(fig, updatefig, interval=1000, blit=False)
plt.show()
# y = randn(5000)

# #
# def init():
# #	line.set_data([], [])
# 	return y,

# def animate(i):
# 	global y
# 	# x = np.linspace(0,limit-1, num = limit)
# 	# cur.execute(query)
# 	# db.commit()
# 	# count = cur.rowcount
# 	# row = cur.fetchall()
# 	# #y = [];
# 	# for value in row:
# 	# 	y.append(value[0])
# 	y = random(1000*i)
# 	#line.set_data(x,y)
# 	#return line
# 	return y

# figure = pyplot.hist(y, bins=50, normed=True)
# # Plot de grafiek
# # anim = animation.FuncAnimation(figure, animate, init_func=init, 
# # 	frames=limit, interval=1000*(1.0/200), blit=False)

#anim.save(uuid+".mp4", fps=30, extra_args=['-vcodec', 'libx264'])


pyplot.show()


