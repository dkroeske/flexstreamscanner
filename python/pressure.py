import MySQLdb
from pylab import *

print "Connecting to footpod database"
db = MySQLdb.connect(host="192.168.2.138", user="footpod", passwd="footpod", db="FootpodAnalyser")
cur = db.cursor()

#
# Get latest session
#
query = """SELECT sessionUuid, created FROM footpod ORDER BY created DESC LIMIT 1"""
#print "query:{:s}".format(query)
cur.execute(query)
row = cur.fetchall()
#print row
for value in row:
	uuid = value[0]
	date = value[1]
print uuid
print date

#
#
query = "select sensor0, sensor1, sensor2, sensor3, sensor4 from pressure where sessionUuid = \'" + uuid +"\'"
print "query:{:s}".format(query)
cur.execute(query)
count = cur.rowcount
row = cur.fetchall()

#
#
x = np.linspace(1, count, count, endpoint=True)

for n in range(0, 5-1):
	y = []
	for value in row:
		y.append(value[n])
	plot(x,y, linewidth=1.0, linestyle="-", label = "Pressure Sensor " + str(n))

# Pressure sensors max value 16 bits
maxx = 0xFFFF
ylim(0, maxx)
yticks([0, 1*0xFFFF/4, 2*0xFFFF/4, 3*0xFFFF/4, 4*0xFFFF/4],
	['0x0000', '0x3FFF]', '0x7FFF', '0xBFFF', '0xFFFF'])


#xticks(np.linspace(1, count, 10, endpoint=True))
#ylim(1, max(y)+100)

legend(loc='upper left')

xlabel('sensorsamples (n)')
ylabel('sensor value (raw hex)')
title("Uncalibrated Pressure Sensors Data\n(session ID:\'" + uuid + "\' @" + str(date)+")")


show()

