import MySQLdb
import numpy as np

class Session:
    uuid = ""
    date = ""

    def __init__(self, uuid, date):
        self.uuid = uuid
        self.date = date
 
class FootpodDB:

    db_host = "192.168.2.138"
    db_user = "footpod"
    db_passwd = "footpod"
    db_scheme = "FootpodAnalyser"

    def __init__(self, host):
        db_host = host
        #print db_host

    def pressures(self, uuid, limit = 10):
        db = MySQLdb.connect(host=self.db_host, user=self.db_user, passwd=self.db_passwd, db=self.db_scheme)
        db.autocommit(True)
        cur = db.cursor()
        query = ("select sensor0, sensor1, sensor2, sensor3, sensor4, sensor5, sensor6, sensor7, sensor8," 
                 "sensor9, sensor10, sensor11, sensor12, sensor13, sensor14, sensor15, sensor16, sensor17,"
                 "sensor18, sensor19 from pressure where sessionUuid = \'" + uuid + "\' "
                 "ORDER BY pressure.packetCounter DESC LIMIT " + str(limit) )
        #print query
        cur.execute(query)
        db.commit()
        count = cur.rowcount
        row = cur.fetchall()

        # Update all, take average of last <limit> samples
        data = np.zeros(20)
        for n in range(0, 20):
            y = [];
            for value in row:
                y.append(value[n])
            average = np.average(y)
            norm = np.average(y) / 0xFFFF
            data[n] = norm
        return data

    def pressureSamples(self, uuid, nrSamples = 100):
        db = MySQLdb.connect(host=self.db_host, user=self.db_user, passwd=self.db_passwd, db=self.db_scheme)
        db.autocommit(True)
        cur = db.cursor()
        query = ("select sensor0, sensor1, sensor2, sensor3, sensor4, sensor5, sensor6, sensor7, sensor8," 
                 "sensor9, sensor10, sensor11, sensor12, sensor13, sensor14, sensor15, sensor16, sensor17,"
                 "sensor18, sensor19 from pressure where sessionUuid = \'" + uuid + "\' "
                 "ORDER BY pressure.packetCounter DESC LIMIT " + str(nrSamples) )
        #print query
        cur.execute(query)
        db.commit()

        samples = cur.fetchall()

        return samples

    def mostRecentSession(self):
        limit = 1
        db = MySQLdb.connect(host=self.db_host, user=self.db_user, passwd=self.db_passwd, db=self.db_scheme)
        db.autocommit(True)
        cur = db.cursor()

        query = ("SELECT sessionUuid, created FROM footpod ORDER BY created DESC LIMIT " + str(limit) )
        cur.execute(query)
        row = cur.fetchall()
        for value in row:
            session = Session( uuid = value[0], date = value[1] )
        return session

# # usage
# fdb = FootpodDB(host = "192.168.2.138")
# s = fdb.mostRecentSession()

# fdb.pressures(uuid = s.uuid)

