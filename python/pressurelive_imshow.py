import MySQLdb
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

sensorid = "0"
dbipaddr = "192.168.2.172"
limit = 10

z = np.zeros([5,5])
print z

#
# image
#
fig = plt.figure()
#im = plt.imshow(z, cmap=plt.get_cmap('gist_gray_r'), vmin=0, vmax=1)
im = plt.imshow(z, cmap=plt.get_cmap('jet'), vmin=0, vmax=1)

#
# Setup db
#
db = MySQLdb.connect(host="192.168.2.172", user="footpod", passwd="footpod", db="FootpodAnalyser")
db.autocommit(True)
cur = db.cursor()

#
# Get latest session
#
query = """SELECT sessionUuid, created FROM footpod ORDER BY created DESC LIMIT 1"""
cur.execute(query)
row = cur.fetchall()
for value in row:
	uuid = value[0]
	date = value[1]
print "Unique session ID: " + uuid
print "Recorded on:" + str(date)

#
# Do query string and print
#
query = "select sensor0, packetCounter from pressure where sessionUuid = \'" + uuid +"\' ORDER BY pressure.packetCounter DESC LIMIT " + str(limit)
print "query string: " + query


def init():
	global z
	print("init")
	z = np.zeros([5,5])
	print z

def update(*args):
	global z
	print "update:" + str(args)
	cur.execute(query)
	db.commit()
	count = cur.rowcount
	row = cur.fetchall()
	y = [];
	for value in row:
		y.append(value[0])
	average = np.average(y)
	norm = np.average(y) / 0xFFFF
	z[2,2] = norm;
	im.set_data(z)
	#print z
	return im


plt.clim
plt.title("Pressure")

anim = animation.FuncAnimation(fig, update, init_func=init, interval=100, blit=False)
anim.save(uuid+".mp4", fps=30, extra_args=['-vcodec', 'libx264'])


plt.show()