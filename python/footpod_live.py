import MySQLdb
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

sensorid = "0"
dbipaddr = "192.168.2.172"
limit = 10

sensorMap = [(147,247), (161,57),  (95,516), (129,400),  (163,132),
			 (178,191), (136,467), (128,313),  (26,188),  (47,135),
			 (43,300),  (48,468),  (71,55),   (51,238),  (54,394),
			 (91,440),  (125,20),  (127,115), (117,192), (98,259)]
#sensorMap = [(50,150)]
#print sensorMap

z = np.zeros([200,540]).T
#print z

#
# image
#
fig = plt.figure()
#im = plt.imshow(z, cmap=plt.get_cmap('gist_gray_r'), vmin=0, vmax=1)
im = plt.imshow(z, cmap=plt.get_cmap('jet'), aspect='equal', alpha=1.0, vmin=0, vmax=1)

#
# Setup db
#
# db = MySQLdb.connect(host="192.168.2.172", user="footpod", passwd="footpod", db="FootpodAnalyser")
# db.autocommit(True)
# cur = db.cursor()

#
# Get latest session
#
# query = """SELECT sessionUuid, created FROM footpod ORDER BY created DESC LIMIT 1"""
# cur.execute(query)
# row = cur.fetchall()
# for value in row:
# 	uuid = value[0]
# 	date = value[1]
# print "Unique session ID: " + uuid
# print "Recorded on:" + str(date)

#
# Do query string and print
#
# query = "select sensor0, packetCounter from pressure where sessionUuid = \'" + uuid +"\' ORDER BY pressure.packetCounter DESC LIMIT " + str(limit)
# print "query string: " + query


def init():
	global z
	print("init")
	z = np.zeros([200,540]).T
	print z

# def update(*args):
# 	global z
# 	print "update:" + str(args)
# 	cur.execute(query)
# 	db.commit()
# 	count = cur.rowcount
# 	row = cur.fetchall()
# 	y = [];
# 	for value in row:
# 		y.append(value[0])
# 	average = np.average(y)
# 	norm = np.average(y) / 0xFFFF
# 	z[2,2] = norm;
# 	im.set_data(z)
# 	#print z
# 	return im

def update(*args):
	global z
	print "update:" + str(args)
	value = ((args[0] % 100)/100.0)
	for coord in sensorMap:
		insertInMap(swapCoord(coord), value, 20)
		#z[x,y] = value
	im.set_data(z)
	#print z
	return im

def insertInMap(coord, value, size = 20):
	dx,dy = coord[0], coord[1]
	for y in range(dy-size/2,dy+size/2):
		for x in range(dx-size/2,dx+size/2):
			z[x,y] = value

def swapCoord(coord):
	return (coord[1], coord[0])

#plt.clim
plt.title("Pressure")
plt.colorbar()


anim = animation.FuncAnimation(fig, update, init_func=init, interval=100, blit=False)


plt.show()