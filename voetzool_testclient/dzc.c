#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <time.h>

void error(const char *msg)
{
    fprintf(stderr,"%s", msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buffer[256];
    if (argc < 4) {
       fprintf(stderr,"Usage %s filename hostname port\n", argv[0]);
       exit(0);
    }
    
    unsigned long fileLength;
    FILE *ptr_file;
    
    if( !(ptr_file = fopen(argv[1],"rb")) )
    {
    	error("Unable top open file");
    }
    fseek(ptr_file, 0, SEEK_END);
    fileLength = ftell(ptr_file);
    fseek(ptr_file, 0, SEEK_SET);
    
 	//    
    portno = atoi(argv[3]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");

    //
    //int i = 1; 
    //(sockfd, IPPROTO_TCP, TCP_NODELAY, (void *)&i, sizeof(i));

    server = gethostbyname(argv[2]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");
    
    printf("-------------------- START --------------------------\n");
    for(unsigned long idx = 0; idx < fileLength; idx++)
    {
    	fread(buffer, sizeof(char), 1, ptr_file);
    	n = write(sockfd, buffer, 1);
    	if(n < 0)
    		error("ERROR writing to socket");
		
        if(!(idx%16) && 0 != idx)
            printf("\n");
        printf("0x%.2X, ",(unsigned char) buffer[0]);
    }
    printf("\n-------------------- END ----------------------------\n");
 
    close(sockfd);
    
    fclose(ptr_file);
    
    return 0;
}
