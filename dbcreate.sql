SELECT footpod;

CREATE TABLE IF NOT EXISTS footpod
(
	uid BIGINT PRIMARY KEY AUTO_INCREMENT, 
	serialNumber BIGINT NOT NULL,
	padType varchar(255),
	created DATETIME,
	description varchar(255) NOT NULL,
	sessionUuid varchar(80) NOT NULL
)TYPE=InnoDB;

CREATE INDEX sessionUuid ON footpod(sessionUuid);


CREATE TABLE IF NOT EXISTS powerinfo
(
	uid BIGINT PRIMARY KEY AUTO_INCREMENT, 
	created DATETIME,
	batteryLevel BIGINT, 
	onCharger BOOL,
	sessionUuid varchar(80) not null,
    FOREIGN KEY fk_footpod(sessionUuid) 
    REFERENCES footpod(sessionUuid) 
        	ON UPDATE CASCADE 
        	ON DELETE CASCADE
)TYPE=InnoDB;

CREATE TABLE IF NOT EXISTS accelerometer_heel
(
	uid BIGINT PRIMARY KEY AUTO_INCREMENT, 
	created DATETIME,
	packetCounter INT,
	x INT, 
	y INT, 
	z INT,
	sessionUuid varchar(80) not null,
    FOREIGN KEY fk_footpod(sessionUuid) 
    REFERENCES footpod(sessionUuid) 
        	ON UPDATE CASCADE 
        	ON DELETE CASCADE
)TYPE=InnoDB;

CREATE TABLE IF NOT EXISTS accelerometer_toe
(
	uid BIGINT PRIMARY KEY AUTO_INCREMENT, 
	created DATETIME,
	packetCounter INT,
	x INT, 
	y INT, 
	z INT,
	sessionUuid varchar(80) not null,
    FOREIGN KEY fk_footpod(sessionUuid) 
    REFERENCES footpod(sessionUuid) 
        	ON UPDATE CASCADE 
        	ON DELETE CASCADE
)TYPE=InnoDB;

CREATE TABLE IF NOT EXISTS temperature
(
	uid BIGINT PRIMARY KEY AUTO_INCREMENT, 
	created DATETIME,
	packetCounter INT,
	temperature INT,
	sessionUuid varchar(80) not null,
    FOREIGN KEY fk_footpod(sessionUuid) 
    REFERENCES footpod(sessionUuid) 
        	ON UPDATE CASCADE 
        	ON DELETE CASCADE
)TYPE=InnoDB;

CREATE TABLE IF NOT EXISTS pressure
(
	uid BIGINT AUTO_INCREMENT, 
	PRIMARY KEY (uid),
	created DATETIME,
	packetCounter INT,
	sensor0 INT, 
	sensor1 INT, 
	sensor2 INT, 
	sensor3 INT, 
	sensor4 INT, 
	sensor5 INT, 
	sensor6 INT, 
	sensor7 INT, 
	sensor8 INT, 
	sensor9 INT, 
	sensor10 INT, 
	sensor11 INT, 
	sensor12 INT, 
	sensor13 INT, 
	sensor14 INT, 
	sensor15 INT, 
	sensor16 INT, 
	sensor17 INT, 
	sensor18 INT, 
	sensor19 INT,
	sessionUuid varchar(80) not null,
    FOREIGN KEY fk_footpod(sessionUuid) 
    REFERENCES footpod(sessionUuid) 
        	ON UPDATE CASCADE 
        	ON DELETE CASCADE
)TYPE=InnoDB;


